package lab5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
public class WordsManager {
    private Text text;

    public WordsManager(Text text) {
        this.text = text;
    }

    public List<String> getStartingWithVowels() {
        List<String> vowelsWords = new ArrayList<>();

        Pattern startingVowelsPattern = Pattern.compile("(?:\s|^)(?ui:[аеєиіїоуюя])[а-яА-яЇїІіЄєҐґ'-]*");
        Matcher matcher = startingVowelsPattern.matcher(text.toString());

        while (matcher.find()) {
            vowelsWords.add(matcher.group().trim());
        }

        return vowelsWords;
    }

    public List<String> getSortedByConsonants(List<String> words) {
        List<String> sorted = new ArrayList<>(words);

        Pattern consonantPattern = Pattern.compile("(?ui:[бвгґджзйклмнпрстфхцчшщ])");

        var comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Matcher matcher1 = consonantPattern.matcher(o1);
                boolean found1 = matcher1.find();

                Matcher matcher2 = consonantPattern.matcher(o2);
                boolean found2 = matcher2.find();

                if (!found1 || !found2) {
                    return o1.toLowerCase().compareTo(o2.toLowerCase());
                }

                String firstConsonant = matcher1.group().toLowerCase();
                String secondConsonant = matcher2.group().toLowerCase();

                return firstConsonant.compareTo(secondConsonant);
            }
        };

        sorted.sort(comparator);

        return sorted;
    }
}
