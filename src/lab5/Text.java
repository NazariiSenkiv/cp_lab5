package lab5;

import java.util.ArrayList;
import java.util.List;

public class Text {
    private List<String> stringList;

    public Text() {
        stringList = new ArrayList<>();
    }
    public Text(List<String> list) {
        stringList = new ArrayList<>(list);
    }

    public void add(String str) {
        stringList.add(str);
    }

    public List<String> getStringList() {
        return new ArrayList<>(stringList);
    }

    @Override
    public String toString() {
        return String.join(" ", stringList);
    }
}
