package lab5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WordsManagerTest {

    static private Text text;

    @BeforeAll
    static void beforeAll() {
        text = new Text();
        text.add("ад'ютант Авфавіт делімітер кеш посилання абориген  агроном");
    }

    @Test
    void testGetStartingWithVowels() {
        // Arrange
        WordsManager wordsManager = new WordsManager(text);
        List<String> expectedResult = new ArrayList<>(List.of("ад'ютант", "Авфавіт", "абориген", "агроном"));

        // Act
        List<String> actualResult = wordsManager.getStartingWithVowels();

        // Assert
        Assertions.assertFalse(actualResult.isEmpty());
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    void testGetSortedByConsonants() {
        // Arrange
        List<String> words = new ArrayList<>(List.of("ад'ютант", "Авфавіт", "абориген", "агроном"));
        WordsManager wordsManager = new WordsManager(text);

        List<String> expectedResult = new ArrayList<>(List.of("абориген", "Авфавіт", "агроном", "ад'ютант"));

        // Act
        List<String> actualResult = wordsManager.getSortedByConsonants(words);

        // Assert
        Assertions.assertEquals(expectedResult, actualResult);
    }
}