package lab5;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            Text text = new Text();

            String input = scanner.nextLine();
            // check to empty input string
            if (input.isEmpty()) {
                System.out.println("Entered empty string");
                return;
            }

            text.add(input);

            WordsManager wordsManager = new WordsManager(text);

            List<String> startingWithVowels = wordsManager.getStartingWithVowels();
            // check whether exists such words
            if (startingWithVowels.size() == 0) {
                System.out.println("Text doesn't contain words, which start with vowels!");
                return;
            }

            List<String> resultWords = wordsManager.getSortedByConsonants(startingWithVowels);

            System.out.println("Result:\n" + String.join("\n", resultWords));
        }
    }
}